import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:simple_get_crud_product/app/data/models/product_model.dart';
import 'package:simple_get_crud_product/app/data/providers/product_provider.dart';

class HomeController extends GetxController {
  var products = List<Product>.empty().obs;

  @override
  void onInit() {
    getAllProduct();
    super.onInit();
  }

  void getAllProduct() {
    ProductProvider().getAllProducts().then((res) {
      res.forEach((key, value) {
        Product resProduct = Product.fromJson(value);
        products.add(resProduct);
      });
    });
  }

  void add(String name) {
    if (name != "") {
      final date = DateTime.now().toIso8601String();

      ProductProvider().postProduct(name, date).then(
        (res) {
          final data = Product(
            id: res.name,
            name: name,
            createdAt: date,
          );
          products.add(data);

          Get.back();
        },
      );
    } else {
      dialogError("TextField is empty!");
    }
  }

  void delete(String id) {
    ProductProvider().deleteProduct(id).then(
          (res) => products.removeWhere((element) => element.id == id),
        );
  }

  Product findProductById(String id) {
    return products.firstWhere((element) => element.id == id);
  }

  void edit(String id, String name) {
    final data = findProductById(id);

    ProductProvider().editProduct(id, name).then((res) {
      data.name = name;

      products.refresh();
      Get.back();
    });
  }

  void dialogError(String msg) {
    Get.defaultDialog(
      title: "Error",
      content: const Text(
        "Please Check Again ...",
        textAlign: TextAlign.center,
      ),
    );
  }
}
