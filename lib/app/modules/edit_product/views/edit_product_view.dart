import 'package:flutter/material.dart';

import 'package:get/get.dart';
import 'package:simple_get_crud_product/app/modules/home/controllers/home_controller.dart';

import '../controllers/edit_product_controller.dart';

class EditProductView extends GetView<EditProductController> {
  EditProductView({super.key});

  final homeController = Get.find<HomeController>();

  @override
  Widget build(BuildContext context) {
    final data = homeController.findProductById(Get.arguments);
    controller.nameController.text = data.name ?? "";

    return Scaffold(
      appBar: AppBar(
        title: const Text('EDIT PRODUCT'),
        centerTitle: true,
      ),
      body: Padding(
        padding: const EdgeInsets.all(24.0),
        child: ListView(
          children: [
            TextField(
              autocorrect: false,
              controller: controller.nameController,
              decoration: const InputDecoration(
                labelText: "Product Name",
                hintText: "Input product name here ...",
                border: OutlineInputBorder(),
              ),
              onEditingComplete: () => homeController.edit(
                  Get.arguments, controller.nameController.text),
            ),
            const SizedBox(height: 24),
            ElevatedButton(
              onPressed: () => homeController.edit(
                  Get.arguments, controller.nameController.text),
              child: const Text("Edit Product"),
            )
          ],
        ),
      ),
    );
  }
}
