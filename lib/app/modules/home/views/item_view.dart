import 'package:flutter/material.dart';

import 'package:get/get.dart';
import 'package:simple_get_crud_product/app/data/models/product_model.dart';
import 'package:simple_get_crud_product/app/modules/home/controllers/home_controller.dart';
import 'package:simple_get_crud_product/app/routes/app_pages.dart';

class ItemView extends GetView {
  final Product? item;
  const ItemView({Key? key, this.item}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ListTile(
      onTap: () => Get.toNamed(Routes.EDIT_PRODUCT, arguments: item?.id ?? ""),
      leading: CircleAvatar(child: Text((item?.name ?? "").substring(0, 1).toUpperCase())),
      title: Text(item?.name ?? ""),
      subtitle: Text(item?.createdAt ?? ""),
      trailing: IconButton(
        onPressed: () {
          Get.find<HomeController>().delete(item?.id ?? "");
        },
        icon: const Icon(Icons.delete),
      ),
    );
  }
}
