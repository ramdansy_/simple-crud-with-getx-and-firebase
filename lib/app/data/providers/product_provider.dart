import 'package:flutter/foundation.dart';
import 'package:get/get.dart';

import '../models/product_model.dart';

class ProductProvider extends GetConnect {
  String url =
      "https://simple-getx-crud-product-default-rtdb.asia-southeast1.firebasedatabase.app";

  Future<Map<String, dynamic>> getAllProducts() async {
    final response = await get('$url/product.json');
    return response.body;
  }

  Future<void> editProduct(String id, String name) async {
    final response = await patch('$url/product/$id.json', {
      "name": name,
    });
    return response.body;
  }

  Future<Product> postProduct(String name, String date) async {
    final response = await post('$url/product.json', {
      "name": name,
      "createdAt": date,
    });

    return Product.fromJson(response.body);
  }

  Future<void> deleteProduct(String id) async =>
      await delete('$url/product/$id.json');
}
