import 'package:flutter/material.dart';

import 'package:get/get.dart';
import 'package:simple_get_crud_product/app/modules/home/views/item_view.dart';
import 'package:simple_get_crud_product/app/routes/app_pages.dart';

import '../controllers/home_controller.dart';

class HomeView extends GetView<HomeController> {
  const HomeView({Key? key}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('ALL PRODUCTS'),
        centerTitle: true,
        actions: [
          IconButton(
            onPressed: () => Get.toNamed(Routes.ADD_PRODUCT),
            icon: const Icon(Icons.add),
          ),
        ],
      ),
      body: Obx(
        () => controller.products.isEmpty
            ? const Center(
                child: Text("Tidak ada data"),
              )
            : ListView.builder(
                itemCount: controller.products.length,
                itemBuilder: (context, index) {
                  var item = controller.products[index];
                  return ItemView(item: item);
                },
              ),
      ),
    );
  }
}
