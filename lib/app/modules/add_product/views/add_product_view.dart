import 'package:flutter/material.dart';

import 'package:get/get.dart';
import 'package:simple_get_crud_product/app/modules/home/controllers/home_controller.dart';

import '../controllers/add_product_controller.dart';

class AddProductView extends GetView<AddProductController> {
  const AddProductView({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('ADD PRODUCT'),
        centerTitle: true,
      ),
      body: Padding(
        padding: const EdgeInsets.all(24.0),
        child: ListView(
          children: [
            TextField(
              autocorrect: false,
              controller: controller.nameController,
              decoration: const InputDecoration(
                labelText: "Product Name",
                hintText: "Input product name here ...",
                border: OutlineInputBorder(),
              ),
              onEditingComplete: () => Get.find<HomeController>()
                  .add(controller.nameController.text),
            ),
            const SizedBox(height: 24),
            ElevatedButton(
              onPressed: () => Get.find<HomeController>()
                  .add(controller.nameController.text),
              child: const Text("Add Product"),
            )
          ],
        ),
      ),
    );
  }
}
